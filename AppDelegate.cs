﻿using Foundation;
using UIKit;
using System.IO;
using System;
using MBProgressHUD;
using Newtonsoft.Json;
using System.Xml;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Reflection;
using System.Text;
using CoreGraphics;
using System.Drawing;
using RangeSlider;

namespace IoTFinder
{

	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
	[Register ("AppDelegate")]
	public class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations

		public override UIWindow Window {
			get;
			set;
		}

		private MTMBProgressHUD _mbProgressHud = new MTMBProgressHUD();

		public MTMBProgressHUD HUD
		{
			get {
				_mbProgressHud.RemoveFromSuperViewOnHide = true;
				return _mbProgressHud;
			}
			set {
				_mbProgressHud = value;		
			}
		}

		private HomeController _home;

		public override bool FinishedLaunching (UIApplication application, NSDictionary launchOptions)
		{
			// Override point for customization after application launch.
			// If not required for your application you can safely delete this method
			Window = new UIWindow(UIScreen.MainScreen.Bounds);

			Window.MakeKeyAndVisible ();

			_home = new HomeController();

			Window.RootViewController = _home;
			Window.RootViewController.View = _home.View;

			OpenDatabase ();

			return true;
		}

		public SQLiteConnection database;
		public MyAlertViewDelegate alertDelegate;

		public void OpenDatabase()
		{
			database = new SQLiteConnection (Path.Combine (Path.Combine( Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), "..", "Library"), "IoTDatabase.db"));
			database.Trace = true;
			database.CreateTable<CommunicationFile>();
			database.CreateTable<PropertyValue>();
			database.CreateTable<KeywordEntry>();
			database.CreateTable<KeywordManualEntry>();
		}

		public override void OnResignActivation (UIApplication application)
		{
			// Invoked when the application is about to move from active to inactive state.
			// This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
			// or when the user quits the application and it begins the transition to the background state.
			// Games should use this method to pause the game.
		}

		NSUrl url;

		public override bool OpenUrl (UIApplication app, NSUrl url, NSDictionary options)
		{
			
			if (url.IsFileUrl) 
			{
				if (url.PathExtension.EndsWith ("db")) 
				{
					Uri uri = new Uri (url.Path);


					string filename = System.IO.Path.GetFileName(uri.LocalPath);


					var alert1 = new UIAlertView ();
					alert1.Title = "Import" + " " + filename + " " + "?";
					alert1.Message = "";
					alert1.AddButton ("Import");
					alert1.AddButton ("Cancel");
					alertDelegate = new MyAlertViewDelegate ();
					alertDelegate.button0Clicked += delegate {

				
						var path = Path.Combine (Path.Combine( Environment.GetFolderPath (Environment.SpecialFolder.MyDocuments), "..", "Library"), "IoTDatabase.db");
						File.Copy (url.Path, path, true);


					};
					alert1.DismissWithClickedButtonIndex (1, true);
					alert1.Delegate = alertDelegate;
					alert1.Show ();
				}
				else {
					this.url = url;
					Uri uri = new Uri (url.Path);

					string filename = System.IO.Path.GetFileName (uri.LocalPath);
					string type = System.IO.Path.GetExtension (uri.LocalPath);

					var alert1 = new UIAlertView ();
					alert1.Title = filename;
					alert1.Message = "Extension: " + type;
					alert1.AddButton ("Import Data");
					alert1.AddButton ("Discover Device");
					alert1.AddButton ("Cancel");
					alertDelegate = new MyAlertViewDelegate ();

					alertDelegate.button0Clicked += AlertDelegate_ImportClicked; 

					alertDelegate.button1Clicked += AlertDelegate_DiscoverClicked; 

					alert1.DismissWithClickedButtonIndex (1, true);
					alert1.Delegate = alertDelegate;
					alert1.Show ();	
				}
			}

			return true;
		}

		RangeSliderView rangeSlider;

		void AlertDelegate_DiscoverClicked (object sender, EventArgs e)
		{
			importView = new UIViewController ();
			importView.View = new UIView(new CGRect (0, 0, 300, 500));
			importView.View.BackgroundColor = UIColor.White;

			title = new UILabel ();
			title.Frame = new CGRect (20, 20, 260, 44);
			title.Text = "DISCOVER DEVICE";
			title.TextAlignment = UITextAlignment.Center;
			title.TextColor = UIColor.Blue;
			importView.View.Add (title);

			var labelA = new UILabel ();
			labelA.Frame = new CGRect (40, 100, 30, 20);
			labelA.Font = UIFont.FromName ("Helvetica-Bold", 16);
			labelA.Text = "A";
			importView.View.Add (labelA);

			var labelB = new UILabel ();
			labelB.Frame = new CGRect (145, 100, 30, 20);
			labelB.Font = UIFont.FromName ("Helvetica-Bold", 16);
			labelB.Text = "B";
			importView.View.Add (labelB);

			var labelC = new UILabel ();
			labelC.Frame = new CGRect (240, 100, 30, 20);
			labelC.Font = UIFont.FromName ("Helvetica-Bold", 16);
			labelC.Text = "C";
			importView.View.Add (labelC);

			var algA = new UILabel ();
			algA.Frame = new CGRect (40, 170, 200, 20);
			algA.Font = UIFont.FromName ("Helvetica", 16);
			algA.Text = "33.3% - Levensthein (A)";
			importView.View.Add (algA);

			var algB = new UILabel ();
			algB.Frame = new CGRect (40, 195, 200, 20);
			algB.Font = UIFont.FromName ("Helvetica", 16);
			algB.Text = "33.3% - TF-IDF Table (B)";
			importView.View.Add (algB);

			var algC = new UILabel ();
			algC.Frame = new CGRect (40, 220, 200, 20);
			algC.Font = UIFont.FromName ("Helvetica", 16);
			algC.Text = "33.3% - Manual Table (C)";
			importView.View.Add (algC);


			rangeSlider = new RangeSliderView {
				Frame = new CGRect (30f, 120f, 240f, 40f),
			};
			rangeSlider.MaxValue = 100;
			rangeSlider.MinValue = 0;
			rangeSlider.Step = 0.1f;
			rangeSlider.LeftValue = 33.3f;
			rangeSlider.RightValue = 66.6f;
			rangeSlider.LeftValueChanged += delegate 
			{
				algA.Text = rangeSlider.LeftValue.ToString("0.0") + "%" + " - Levensthein (A)";
				algB.Text = (rangeSlider.RightValue- rangeSlider.LeftValue).ToString("0.0") + "%" + " - TF-IDF Table (B)" ;
				algC.Text = (100-rangeSlider.RightValue).ToString("0.0") + "%" + " - Manual Table (C)" ;
			};
			rangeSlider.RightValueChanged += delegate 
			{
				algA.Text = rangeSlider.LeftValue.ToString("0.0") + "%" + " - Levensthein (A)";
				algB.Text = (rangeSlider.RightValue- rangeSlider.LeftValue).ToString("0.0") + "%" + " - TF-IDF Table (B)" ;
				algC.Text = (100-rangeSlider.RightValue).ToString("0.0") + "%" + " - Manual Table (C)" ;
			};
			importView.View.AddSubview (rangeSlider);

			importButton = new UIButton (UIButtonType.RoundedRect);
			importButton.SetTitle ("START DISCOVER", UIControlState.Normal);
			importButton.Frame = new CGRect (0, 450, 300, 44);
			importButton.TouchUpInside += delegate {
				DiscoverClicked();
			};
			importView.View.Add (importButton);

			this._home.IphonePresentMenu (importView, new CGPoint ((this.Window.Frame.Width - 300) / 2, (this.Window.Frame.Height - 500) / 2), new CGSize (300, 500), delegate {

			});
		}

		void DiscoverClicked()
		{
		
			Uri uri = new Uri (url.Path);

			string filename = System.IO.Path.GetFileName(uri.LocalPath);
			string type = System.IO.Path.GetExtension(uri.LocalPath);

			try
			{
				var progressHud = SingletonAcessor.Acessor.HUD;
				progressHud.Frame = this.Window.Frame;
				progressHud.Mode = MBProgressHUD.MBProgressHUDMode.Indeterminate;
				progressHud.LabelText = "Trying to discover device";
				progressHud.DetailsLabelText = "Please wait...";
				progressHud.Opacity = (float)0.4;
				this.Window.Add (progressHud);
				progressHud.Show (true, delegate{ Discover(); });
				progressHud.DidHide += ProgressHud_DidHide;

			}
			catch(Exception f)
			{
				Helpers.MostraAlerta("Error", "Impossible to discover device");
			}

		}
	
		void Discover()
		{
			Uri uri = new Uri (url.Path);

			string filename = System.IO.Path.GetFileName (uri.LocalPath);
			string type = System.IO.Path.GetExtension (uri.LocalPath);

			var newDeviceProperties = new List<PropertyValue> ();

			try {
				string content = System.IO.File.ReadAllText (uri.LocalPath);

				bool isXml = type.ToUpper ().Equals (".XML");

				if (isXml) 
				{
				
					XmlDocument xDoc = new XmlDocument ();

					if (content != null) {
						xDoc.LoadXml (content);
					}

				}

				bool isJson = type.ToUpper ().Equals (".JSON");

				if (isJson) {

					JToken node = JToken.Parse (content);

					WalkNode (node, null, prop => {

						if (prop.HasValues && prop.Value.Type == JTokenType.Object) {
							//prop.Value.Select(x=>x.)
							StringBuilder s = new StringBuilder ("");

							foreach (JProperty child in prop.Value.Children<JProperty>()) {
								s.Append (child.Name + ",");
							}

							var propertyValue = new PropertyValue {
								Property = prop.Name,
								Value = s.ToString (),
								Type = "Multiple",
								ParentFile = filename,
							};
							newDeviceProperties.Add (propertyValue);

						} else {
							var propertyValue = new PropertyValue {
								Property = prop.Name,
								Value = prop.Value.ToString (),
								Type = "Single",
								ParentFile = filename,
							};
							newDeviceProperties.Add (propertyValue);
						}
					});

				}


				var db = SingletonAcessor.Acessor.database;
				var typeOfObject = typeof(PropertyValue);
				var mapping =  db.GetMapping(typeOfObject);
				List <PropertyValue> list = db.Query(mapping,"select * from \"" + mapping.TableName + "\"", new string[]{""}).Cast<PropertyValue>().ToList();

				//remover repetidos na propriedade e mesmo nome de dispositivo
				List<PropertyValue> result = list.GroupBy(g => new { g.Property, g.DeviceName })
					.Select(g => g.First())
					.ToList();
				
				var valueResultList = new List<PropertyValue>();
				var propertyResultList = new List<PropertyValue>();


				//VERIFICACAO Levenshtein distance
				foreach (PropertyValue n in newDeviceProperties) {

					foreach (PropertyValue d in result) {

						//TODO add to lower
						if(n.Property.Length>2 && d.Property.Length>2 && Compute(n.Property,d.Property)<2)
						//if(n.Property.Length>3 && d.Property.Length>3 && (Contains(n.Property, d.Property) || Contains(d.Property, n.Property)))
							propertyResultList.Add(d);

//						if(Contains(n.Value, d.Value) || Contains(d.Value, n.Value))
//							valueResultList.Add(d);

					}
				}

				sb = new StringBuilder();

				var o = propertyResultList.GroupBy( i => i.DeviceName ).OrderByDescending(g => g.Count());

				sb.AppendLine();
				sb.AppendLine("DEVICE MATCHING:");
					
				foreach( var grp in o)
				{
					
					sb.AppendLine( string.Format( "{0}: {1}", grp.Key, grp.Count() ));
				}

				var l = propertyResultList.GroupBy( i => i.DeviceType ).OrderByDescending(g => g.Count());

				sb.AppendLine();

				sb.AppendLine("TYPE MATCHING:");

				foreach( var grp in l)
				{
					sb.AppendLine( string.Format( "{0}: {1}", grp.Key, grp.Count() ));
				}

				int h = 0; 
			} 
			catch (Exception f) 
			{
				
				Helpers.MostraAlerta ("Error", "Impossible to discover device");
			}
		
		}

		StringBuilder sb;

		void ProgressHud_DidHide (object sender, EventArgs e)
		{
			SingletonAcessor.Acessor.HUD.DidHide -= ProgressHud_DidHide;

			Helpers.MostraAlerta("Discover Results", sb.ToString());
		}

	

		public bool Contains (string source, string toCheck)
		{
			return 
				source.IndexOf(toCheck, StringComparison.OrdinalIgnoreCase) >= 0;
		}



		UIViewController importView;
		UITextField deviceNameText;
		UILabel title;
		UIButton importButton;
		UILabel categoryLabel;
		UIPickerView categoriesPicker;
		private UIPickerViewDelegate pickerDelegate;
		PickerModelIphone model;

		public List<string> listCategories = new List<string>{
			"Other",
			"Health",
			"Activity",
			"Sensor",
			"Controller",
			"Light",
			"Display",
			"Utility",
			"Security"
		};

		void AlertDelegate_ImportClicked (object sender, EventArgs e)
		{
			importView = new UIViewController ();
			importView.View = new UIView(new CGRect (0, 0, 300, 500));
			importView.View.BackgroundColor = UIColor.White;

			deviceNameText = new UITextField ();
			deviceNameText.Frame = new CGRect (20, 80, 260, 44);
			deviceNameText.Placeholder = "Please enter Device Name..";
			importView.View.Add (deviceNameText);

			title = new UILabel ();
			title.Frame = new CGRect (20, 20, 260, 44);
			title.Text = "IMPORT DATA FILE";
			title.TextAlignment = UITextAlignment.Center;
			title.TextColor = UIColor.Blue;
			importView.View.Add (title);

			categoryLabel = new UILabel ();
			categoryLabel.Frame = new CGRect (20, 140, 260, 44);
			categoryLabel.Text = "Please select a category:";
			categoryLabel.TextColor = UIColor.LightGray;
			importView.View.Add (categoryLabel);

			importButton = new UIButton (UIButtonType.RoundedRect);
			importButton.SetTitle ("IMPORT", UIControlState.Normal);
			importButton.Frame = new CGRect (0, 450, 300, 44);
			importButton.TouchUpInside += ImportButton_TouchUpInside;
			importView.View.Add (importButton);

			categoriesPicker = new UIPickerView ();
			categoriesPicker.Frame = new CGRect (20,180,260,216);
			pickerDelegate = new PickerDelegate (listCategories);
			categoriesPicker.Delegate = pickerDelegate;
			model = new PickerModelIphone (listCategories);
			model.PickerSelected += delegate {

				deviceNameText.ResignFirstResponder();

			};
			categoriesPicker.Model = model;
			categoriesPicker.ReloadAllComponents ();
			importView.View.AddSubview (categoriesPicker);

			this._home.IphonePresentMenu (importView, new CGPoint ((this.Window.Frame.Width - 300) / 2, (this.Window.Frame.Height - 500) / 2), new CGSize (300, 500), delegate {
				
			});

		}

		string deviceName;
		string deviceType;

		void Import()
		{
		

			Uri uri = new Uri (url.Path);

			string filename = System.IO.Path.GetFileName (uri.LocalPath);
			string type = System.IO.Path.GetExtension (uri.LocalPath);

			try {
				string content = System.IO.File.ReadAllText (uri.LocalPath);

				bool isXml = type.ToUpper ().Equals (".XML");

				if (isXml) {
					XmlDocument xDoc = new XmlDocument ();
					if (content != null) {
						xDoc.LoadXml (content);
					}

				}

				bool isJson = type.ToUpper ().Equals (".JSON");

				if (isJson) {

					JToken node = JToken.Parse (content);

					WalkNode (node, null, prop => {

						if (prop.HasValues && prop.Value.Type == JTokenType.Object) {
							//prop.Value.Select(x=>x.)
							StringBuilder s = new StringBuilder ("");

							foreach (JProperty child in prop.Value.Children<JProperty>()) {
								s.Append (child.Name + ",");
							}

							var propertyValue = new PropertyValue {
								Property = prop.Name,
								Value = s.ToString (),
								Type = "Multiple",
								ParentFile = filename,
								DeviceName = deviceName,
								DeviceType = deviceType
							};
							SingletonAcessor.Acessor.database.Insert (propertyValue);

						} else {
							var propertyValue = new PropertyValue {
								Property = prop.Name,
								Value = prop.Value.ToString (),
								Type = "Single",
								ParentFile = filename,
								DeviceName = deviceName,
								DeviceType = deviceType
							};
							SingletonAcessor.Acessor.database.Insert (propertyValue);
						}
					});

				}

				SingletonAcessor.Acessor.database.Insert (new CommunicationFile {
					fileContent = content,
					filename = filename,
					isXML = isXml,
					isJson = isJson,
					DeviceName = deviceName
				});


				this.InvokeOnMainThread( delegate {
					this._home.HideMenu();
					Helpers.MostraAlerta ("Success", "Communication file sucessfully imported to database");

				});

			} 
			catch (Exception f) {
				this.InvokeOnMainThread (delegate {

					this._home.HideMenu ();

					Helpers.MostraAlerta ("Error", "Impossible to import file");
				});

			}
		}
		void ImportButton_TouchUpInside (object sender, EventArgs e)
		{
			deviceName = deviceNameText.Text;
			deviceType = listCategories [(int)categoriesPicker.SelectedRowInComponent (0)];

			try
			{
				var progressHud = SingletonAcessor.Acessor.HUD;
				progressHud.Frame = this.Window.Frame;
				progressHud.Mode = MBProgressHUD.MBProgressHUDMode.Indeterminate;
				progressHud.LabelText = "Importing data";
				progressHud.DetailsLabelText = "Please wait...";
				progressHud.Opacity = (float)0.4;
				this.Window.Add (progressHud);
				progressHud.Show (true, delegate{ Import(); });

			}
			catch(Exception f)
			{
				Helpers.MostraAlerta("Error", "Impossible to import file");
			}

				

		}

		private static void WalkNode(JToken node,
			Action<JObject> objectAction = null,
			Action<JProperty> propertyAction = null)
		{
			if (node.Type == JTokenType.Object)
			{
				if (objectAction != null) objectAction((JObject) node);

				foreach (JProperty child in node.Children<JProperty>())
				{
					if (propertyAction != null) propertyAction(child);
					WalkNode(child.Value, objectAction, propertyAction);
				}
			}
			else if (node.Type == JTokenType.Array)
			{
				foreach (JToken child in node.Children())
				{
					WalkNode(child, objectAction, propertyAction);
				}
			}
		}

		public string RemoveLast(string text, string character)
		{
			if(text.Length < 1) return text;
			return text.Remove(text.ToString().LastIndexOf(character), character.Length);
		}

	
		public override void DidEnterBackground (UIApplication application)
		{
			// Use this method to release shared resources, save user data, invalidate timers and store the application state.
			// If your application supports background exection this method is called instead of WillTerminate when the user quits.
		}

		public override void WillEnterForeground (UIApplication application)
		{
			// Called as part of the transiton from background to active state.
			// Here you can undo many of the changes made on entering the background.
		}

		public override void OnActivated (UIApplication application)
		{
			// Restart any tasks that were paused (or not yet started) while the application was inactive. 
			// If the application was previously in the background, optionally refresh the user interface.
		}

		public override void WillTerminate (UIApplication application)
		{
			// Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
		}

		public UIFont GetHelveticaBold(float size) 
		{
			UIFont font = UIFont.FromName("Helvetica-Bold",size);
			return font;
		}

		public UIFont GetHelvetica(float size) 
		{
			UIFont font = UIFont.FromName("Helvetica Neue",size);
			return font;
		}

		public bool isIphone 
		{
			get 
			{ 
				return UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone; 
			}
		}

		/// <summary>
		/// Compute the distance between two strings.
		/// </summary>
		public int Compute(string s, string t)
		{
			int n = s.Length;
			int m = t.Length;
			int[,] d = new int[n + 1, m + 1];

			// Step 1
			if (n == 0)
			{
				return m;
			}

			if (m == 0)
			{
				return n;
			}

			// Step 2
			for (int i = 0; i <= n; d[i, 0] = i++)
			{
			}

			for (int j = 0; j <= m; d[0, j] = j++)
			{
			}

			// Step 3
			for (int i = 1; i <= n; i++)
			{
				//Step 4
				for (int j = 1; j <= m; j++)
				{
					// Step 5
					int cost = (t[j - 1] == s[i - 1]) ? 0 : 1;

					// Step 6
					d[i, j] = Math.Min(
						Math.Min(d[i - 1, j] + 1, d[i, j - 1] + 1),
						d[i - 1, j - 1] + cost);
				}
			}
			// Step 7
			return d[n, m];
		}
	}
}


