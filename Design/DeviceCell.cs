using System;
using UIKit;
using CoreGraphics;
using CoreAnimation;

namespace IoTFinder
{
	public class DeviceCell: UITableViewCell
	{
		public UILabel tituloLabel = null;
		public UILabel label2 = null;
		public UILabel label3 = null;
		public UILabel label4 = null;
		public UIImageView selected = null;

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			//this.LayoutMargins = UIEdgeInsets.Zero;

		}
		public DeviceCell (PropertyValue property, int row)
		{

			this.Frame = new CGRect (0, 0, 355, 40);
			this.BackgroundColor = UIColor.White;

			if (row % 2 != 0) {
			
				UIView viewB = new UIView (this.Frame);
				viewB.BackgroundColor = UIColor.LightGray;
				viewB.Alpha = 0.1f;
				this.Add (viewB);
			} 

			tituloLabel = new UILabel ();
			tituloLabel.Frame = SingletonAcessor.Acessor.isIphone? new CGRect (5, 3, 240, 20) : new CGRect (10, 0, 280, 44);
			tituloLabel.Font =  SingletonAcessor.Acessor.isIphone? SingletonAcessor.Acessor.GetHelvetica (9) :SingletonAcessor.Acessor.GetHelvetica (14);
			tituloLabel.Text = property.Property;
			this.Add (tituloLabel);

			label2 = new UILabel ();
			label2.Frame = SingletonAcessor.Acessor.isIphone? new CGRect (5, 17, 240, 20) : new CGRect (310, 0, 180, 44);
			label2.Font = SingletonAcessor.Acessor.isIphone? SingletonAcessor.Acessor.GetHelvetica (9) :SingletonAcessor.Acessor.GetHelvetica (14);
			label2.TextColor = SingletonAcessor.Acessor.isIphone? UIColor.FromRGB (77, 79, 83): UIColor.DarkTextColor;
			label2.Text = property.Value;
			this.Add (label2);

			label3 = new UILabel ();
			label3.Frame = SingletonAcessor.Acessor.isIphone? new CGRect (255, 3, 95, 20) :new CGRect (510, 0, 160, 44);
			label3.Font =  SingletonAcessor.Acessor.isIphone? SingletonAcessor.Acessor.GetHelvetica (9) :SingletonAcessor.Acessor.GetHelvetica (14);
			label3.Text = property.DeviceName + " ("+property.DeviceType+")";
			this.Add (label3);

			label4 = new UILabel ();
			label4.Frame = SingletonAcessor.Acessor.isIphone? new CGRect (255, 17, 95, 20) : new CGRect (690, 0, 160, 44);
			label4.Font = SingletonAcessor.Acessor.isIphone? SingletonAcessor.Acessor.GetHelvetica (9) :SingletonAcessor.Acessor.GetHelvetica (14);
			label4.TextColor = SingletonAcessor.Acessor.isIphone? UIColor.FromRGB (77, 79, 83): UIColor.DarkTextColor;
			label4.Text = property.Type;
			this.Add (label4);




			UIView view = new UIView(this.Frame);
			if (row % 2 != 0) {
				UIView viewBa = new UIView (this.Frame);
				viewBa.BackgroundColor = UIColor.FromRGB(0,103,163);
				viewBa.Alpha = 0.1f;
				view.Add (viewBa);
			}
			else
				view.BackgroundColor = UIColor.White;

			this.SelectedBackgroundView = view;

			if (SingletonAcessor.Acessor.isIphone) {
				for (int i = 0; i < 1; i++) {

					UIView divisor = new UIView ();
					divisor.BackgroundColor = UIColor.FromRGB (230, 230, 230);


					if (i == 0)
						divisor.Frame = new CGRect (250-1, 0, 1, 40);


					this.Add (divisor);
				}

			} else {
				for (int i = 0; i < 4; i++) {

					UIView divisor = new UIView ();
					divisor.BackgroundColor = UIColor.FromRGB (230, 230, 230);

					if (i == 0)
						divisor.Frame = new CGRect (300-1, 0, 1, 44);

					if (i == 1)
						divisor.Frame = new CGRect (500-1, 0, 1, 44);

					if (i == 2)
						divisor.Frame = new CGRect (680-1, 0, 1, 44);

					if (i == 3)
						divisor.Frame = new CGRect (860-1, 0, 1, 44);

					this.Add (divisor);
				}
			}
		}

		public void SetCorners(UIRectCorner corners, float cornerRadius)
		{
			var maskLayer = new CAShapeLayer();
			maskLayer.Frame = Bounds;
			var roundedPath = UIBezierPath.FromRoundedRect(maskLayer.Bounds, corners, new CGSize(cornerRadius, cornerRadius));
			maskLayer.FillColor = UIColor.White.CGColor;
			maskLayer.BackgroundColor = UIColor.Clear.CGColor;
			maskLayer.Path = roundedPath.CGPath;

			//Don't add masks to layers already in the hierarchy!
			Layer.Mask = maskLayer;
		}
	}
}

