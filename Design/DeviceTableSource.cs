using System;
using UIKit;
using Foundation;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using CoreAnimation;
using CoreGraphics;


namespace IoTFinder
{

	public class DeviceTableSource : UITableViewSource
    {
        static NSString kCellIdentifier = new NSString ("fieldIdentifier");
		private List<PropertyValue> list;
		private string title;
		public event EventHandler RowClicked;
		public event EventHandler RowDeleted;

		public PropertyValue fieldSelected = null;
		public event EventHandler KitHeaderClicked;
		string registerCode;

		public DeviceTableSource (List<PropertyValue> list)
        {
			this.title = title;
			this.list = list;
        }
			
		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}
		
        public override nint RowsInSection (UITableView tableview, nint section)
        {
            return list.Count;
        }

		public override string TitleForFooter (UITableView tableView, nint section)
		{
			return "";
		}
		
		public override string TitleForHeader (UITableView tableView, nint section)
		{
			return "";
	  	}

		public override bool CanEditRow (UITableView tableView, NSIndexPath indexPath)
		{
			return true;
		}		

		public override UITableViewCellEditingStyle EditingStyleForRow (UITableView tableView, NSIndexPath indexPath)
		{
			return UITableViewCellEditingStyle.Delete;
		}

		bool DeleteSucessfull = false;

	

		List<UIButton> buttons;
		public override UIView GetViewForHeader (UITableView tableView, nint section)
		{
			var view =  new UIViewCustomRounded ();
			view.Frame = new CGRect (0, 0, 355, 40) ;
			view.SetCorners(UIRectCorner.TopLeft | UIRectCorner.TopRight, 4);

			List<string> titles = new List<string> {
				"PROPERTY" +"\n" + "VALUE",
				"DEVICE (TYPE)" + "\n" + "VALUE TYPE"
			};

			buttons = new List<UIButton> ();

				for (int i = 0; i < 2; i++) {

					UIButton button = new UIButton (UIButtonType.Custom);


					if (i == 0)
						button.Frame = new RectangleF (0, 0, 250 - 1, 30);

					if (i == 1)
						button.Frame = new RectangleF (250, 0, 105, 30);

			

					button.SetTitle (titles [i], UIControlState.Normal);
					button.SetTitleColor (UIColor.White, UIControlState.Normal);
					button.BackgroundColor = UIColor.FromRGB(0,103,163);;
					button.UserInteractionEnabled = true;
					button.TitleLabel.LineBreakMode = UILineBreakMode.WordWrap;
					button.TitleLabel.Lines = 2;
					button.Tag = i;
					button.HorizontalAlignment = UIControlContentHorizontalAlignment.Left;
					button.ContentEdgeInsets = new UIEdgeInsets (0, 5, 0, 0);
					button.Font = SingletonAcessor.Acessor.GetHelveticaBold (9);
					button.TouchUpInside += delegate {
						if (KitHeaderClicked != null)
							KitHeaderClicked (this, null);
					};

					buttons.Add (button);
				}
			
			foreach (UIButton b in buttons) {
				view.AddSubview (b);
			}

			view.BackgroundColor = UIColor.FromRGB(230,230,230);

			return view;

		}

        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
        {
			var item = list[indexPath.Row];

			DeviceCell cell = new DeviceCell(item,indexPath.Row);

			if(indexPath.Row == list.Count-1)
				cell.SetCorners(UIRectCorner.BottomLeft | UIRectCorner.BottomRight, 4);
			//cell.LayoutMargins = UIEdgeInsets.Zero;
			//cell.PreservesSuperviewLayoutMargins = false;
			cell.Accessory = UITableViewCellAccessory.None;

			return cell;
        }

		public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
		{
			var item = list[indexPath.Row];

			if (RowClicked != null)
				RowClicked (this, null);
			
			tableView.ReloadData ();
		}

    }

	public class UIViewCustomRounded: UIView
	{

		public UIViewCustomRounded (): base()
		{
		}


		public void SetCorners(UIRectCorner corners, float cornerRadius)
		{
			var maskLayer = new CAShapeLayer();
			maskLayer.Frame = Bounds;
			var roundedPath = UIBezierPath.FromRoundedRect(maskLayer.Bounds, corners, new CGSize(cornerRadius, cornerRadius));
			maskLayer.FillColor = UIColor.White.CGColor;
			maskLayer.BackgroundColor = UIColor.Clear.CGColor;
			maskLayer.Path = roundedPath.CGPath;

			//Don't add masks to layers already in the hierarchy!
			Layer.Mask = maskLayer;
		}
	}


}
