using System;
using UIKit;
using MessageUI;
using System.Net.Mail;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using Foundation;
using System.Text;

namespace IoTFinder
{
	public class MyAlertViewDelegate : UIAlertViewDelegate
	{
		public event EventHandler button0Clicked;
		public event EventHandler button1Clicked;
		public event EventHandler button2Clicked;

		public MyAlertViewDelegate()
		{

		}
		
		public override void Clicked (UIAlertView alertview, nint buttonIndex)
		{
			if (buttonIndex == 0 && button0Clicked != null)
				button0Clicked (this, null);

			if (buttonIndex == 1 && button1Clicked != null)
				button1Clicked (this, null);

			if (buttonIndex == 2 && button2Clicked != null)
				button2Clicked (this, null);
		}
	}
}

