using System;
using UIKit;
using System.Collections.Generic;
using Foundation;
using CoreText;

namespace IoTFinder
{
	

	public class PickerModelIphone: UIPickerViewModel
	{
		private List<string> lista = null;
		public event EventHandler PickerSelected;

		public PickerModelIphone (List<string> lista)
		{
			this.lista=lista;
		}

		public override nint GetComponentCount (UIPickerView picker)
		{
			return 1;		
		}

		public override string GetTitle (UIPickerView picker, nint row, nint component)
		{
			return lista[(int)row];
		}

		public override nint GetRowsInComponent(UIPickerView picker, nint component)
		{
			return lista.Count;
		}

		public override void Selected (UIPickerView pickerView, nint row, nint component)
		{
			if (PickerSelected != null)
				PickerSelected (this, null);
		}

		public override UIView GetView (UIPickerView pickerView, nint row, nint component, UIView view)
		{
			UILabel label = new UILabel();
			label.Frame = new CoreGraphics.CGRect (0, 0, pickerView.Frame.Width, 20);
			label.TextAlignment = UITextAlignment.Center;
			label.Text = lista [(int)row];
			label.Font = UIFont.FromName ("Helvetica Neue", 16);
			return (label);

		}
	}

	public class PickerDelegate : UIPickerViewDelegate
	{
		List<string> list;

		public PickerDelegate(List<string> list)
		{
			this.list = list;
		}


		public override NSAttributedString GetAttributedTitle (UIPickerView pickerView, nint row, nint component)
		{

			CTFont _font=new CTFont("Helvetica Neue",32);

			CTStringAttributes att = new CTStringAttributes ();
			att.ForegroundColor = UIColor.White.CGColor;
			att.Font = _font;

			var attString = new NSAttributedString (list[(int)row], att);
			return attString;
		}

		public override UIView GetView (UIPickerView pickerView, nint row, nint component, UIView view)
		{
			UILabel label = new UILabel();
			label.Frame = view.Frame;
			label.Text = list [(int)row];
			label.Font = UIFont.FromName ("Helvetica Neue", 14);
			return (label);

		}

		public override void Selected (UIPickerView pickerView, nint row, nint component)
		{

		}


	}
}

