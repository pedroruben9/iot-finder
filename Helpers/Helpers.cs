using System;
using UIKit;
using System.Text;
using IoTFinder;
using System.Collections.Generic;
using System.Linq;


namespace IoTFinder
{
	public class Helpers
	{

		public static void MostraAlerta(String titulo, String mensagem)
		{
			SingletonAcessor.Acessor.InvokeOnMainThread(delegate{
				UIAlertView alert = new UIAlertView();
				if(titulo!=null)
					alert.Title = titulo;
				alert.AddButton("Ok");
				if(mensagem!=null)
					alert.Message = mensagem;
				alert.Show();
			});


		}


		
	}
}

