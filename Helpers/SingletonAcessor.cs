using System;
using UIKit;

namespace IoTFinder
{
	/// <summary>
	/// Helper to access appdelegat singleton all over the aplication
	/// </summary>
	public static class SingletonAcessor
	{
		/// <summary>
		/// Return the appdelegate 
		/// </summary>
		/// <value>
		/// the aplication appdelegate
		/// </value>
		public static AppDelegate Acessor 
		{
			get { return ((AppDelegate)UIApplication.SharedApplication.Delegate); }
		}

	}
}

