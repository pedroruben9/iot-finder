﻿using System;
using UIKit;
using System.Drawing;
using System.Text;
using System.Globalization;
using CoreGraphics;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using QuickLook;

namespace IoTFinder
{
	public class HomeController : UIViewController
	{
		public HomeController()
		{
		}

		UIImageView imageISCTE;
		UIImageView imageMuzzley;

		UILabel labelTitulo;

		UIButton buttonVisualize;
		UIButton buttonImport;
		UIButton buttonCalculate;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

			labelTitulo = new UILabel (new CGRect (10,120,355,30));
			labelTitulo.TextAlignment = UITextAlignment.Center;
			labelTitulo.Text = "Master Thesis - 2016";
			labelTitulo.Font = UIFont.FromName ("Helvetica", 18);
			this.Add (labelTitulo);

			this.View.BackgroundColor = UIColor.White;

			imageISCTE = new UIImageView ();
			imageISCTE.Frame = new CGRect (42,40,280,78);
			imageISCTE.Image = UIImage.FromFile ("Images/Iscte.png");
			this.Add (imageISCTE);

			imageMuzzley = new UIImageView ();
			imageMuzzley.Frame = new CGRect (56,550,252,70);
			imageMuzzley.Image = UIImage.FromFile ("Images/muzzley.png");
			this.Add (imageMuzzley);

			buttonVisualize = new UIButton (UIButtonType.RoundedRect);
			buttonVisualize.Frame = new CGRect (100, 220, 164, 30);
			buttonVisualize.SetTitle ("CHECK IOT DATA", UIControlState.Normal);
			buttonVisualize.TouchUpInside += delegate 
			{
				CheckIotData();
			};
			this.Add (buttonVisualize);

			buttonImport = new UIButton (UIButtonType.RoundedRect);
			buttonImport.Frame = new CGRect (100, 320, 164, 30);
			buttonImport.SetTitle ("IOT DATA STATS", UIControlState.Normal);
			buttonImport.TouchUpInside += delegate 
			{
				CheckDataStats();
			};
			this.Add (buttonImport);

			buttonCalculate = new UIButton (UIButtonType.RoundedRect);
			buttonCalculate.Frame = new CGRect (50, 420, 264, 30);
			buttonCalculate.SetTitle ("TF-IDF / MANUAL TABLE", UIControlState.Normal);
			buttonCalculate.TouchUpInside += delegate 
			{
				CheckTables();
			};
			this.Add (buttonCalculate);
		}

		void CalculateKeyValues()
		{
			var progressHud = SingletonAcessor.Acessor.HUD;
			progressHud.Frame = this.View.Frame;
			progressHud.Mode = MBProgressHUD.MBProgressHUDMode.Indeterminate;
			progressHud.LabelText = "Creating TF-IDF Values";
			progressHud.DetailsLabelText = "Please wait...";
			progressHud.Opacity = (float)0.4;
			this.View.Add (progressHud);
			progressHud.Show (true, delegate{ CalculateTFIDF(); });
			progressHud.DidHide += FinishedTFIDF;

		
		}

		void FinishedTFIDF (object sender, EventArgs e)
		{
			SingletonAcessor.Acessor.HUD.DidHide -=	FinishedTFIDF;
		}
		
		void CalculateTFIDF()
		{
		
			var db = SingletonAcessor.Acessor.database;
			var typeOfObject = typeof(PropertyValue);
			var mapping =  db.GetMapping(typeOfObject);
			List <PropertyValue> listOfProperties = db.Query(mapping,"select * from \"" + mapping.TableName + "\"", new string[]{""}).Cast<PropertyValue>().ToList();

			//Remove number properties
			listOfProperties = listOfProperties.Where (x => !Regex.IsMatch (x.Property, @"^\d+$")).Cast<PropertyValue>().ToList();

			var type2 = typeof(CommunicationFile);
			var mapping2 =  db.GetMapping(type2);
			var list2 = db.Query(mapping2,"select * from \"" + mapping2.TableName + "\"", new string[]{""}).Cast<CommunicationFile>().ToList();

			var type3 = typeof(KeywordEntry);
			var mapping3 =  db.GetMapping(type3);
			db.Query(mapping3,"delete from \"" + mapping3.TableName + "\"", new string[]{""});


			var listOfCategories = SingletonAcessor.Acessor.listCategories;
			int totalDocuments = list2.Count;

			foreach (string category in listOfCategories) {

				Console.WriteLine ("Started Category " + category);

					
					var listWordsinCategory = listOfProperties.Where (x => x.DeviceType != null && x.DeviceType.Equals (category));

					var wordsCounterList = listWordsinCategory.GroupBy (i => i.Property, StringComparer.InvariantCultureIgnoreCase);

					foreach (var word in wordsCounterList) {
						List<PropertyValue> noDups = listOfProperties.GroupBy (d => new {d.Property, d.ParentFile})
						.Select (d => d.First ())
						.ToList ();


						//Algoritmh with documents
						int numberOfDocumentsWithWord = noDups.Where (f => f.DeviceType!=null && !f.DeviceType.Equals (category)).Count (x => x.Property.Equals (word.Key, StringComparison.InvariantCultureIgnoreCase));
						var TF = word.Count ();
						var IDF = Math.Log (totalDocuments / 1 + numberOfDocumentsWithWord);

						//Algoritmh with properties
						//					int numberOfDocumentsWithWord = listOfProperties.Count (x => x.Property.Equals (word.Key, StringComparison.InvariantCultureIgnoreCase));
						//					var TF = 1;
						//					var IDF = Math.Log(listOfProperties.Count / 1 + numberOfDocumentsWithWord);

						//var rate = TF * IDF;
						double value = 1d / (1d + numberOfDocumentsWithWord);
						var rate = TF * value;


						KeywordEntry newKeywordEntry = new KeywordEntry ();
						newKeywordEntry.Category = category;
						newKeywordEntry.Keyword = word.Key;
						newKeywordEntry.Rate = rate;
						newKeywordEntry.RateCalculus = TF.ToString () + " x ( 1 / 1 + " + numberOfDocumentsWithWord.ToString () + ")";

						SingletonAcessor.Acessor.database.Insert (newKeywordEntry);

					}
				}



		}

		UIView t;
		UITextView text;
		UITableView dataTable;
		UILabel totalProperties;
		UILabel totalImported;

		void CheckIotData()
		{
			t = new UIView (this.View.Frame);

			UIView BackgroundImg = new UIView ();
			BackgroundImg.BackgroundColor = UIColor.White;
			nfloat BackgroundSize = (this.View.Frame.Width > this.View.Frame.Height) ? this.View.Frame.Width : this.View.Frame.Height;
			BackgroundImg.Frame = new CoreGraphics.CGRect (0.0F, 0.0F, BackgroundSize, BackgroundSize);
			this.t.AddSubview (BackgroundImg);

			var db = SingletonAcessor.Acessor.database;
			var type = typeof(PropertyValue);
			var mapping =  db.GetMapping(type);
			var list = db.Query(mapping,"select * from \"" + mapping.TableName + "\"", new string[]{""}).Cast<PropertyValue>().ToList();

			var type2 = typeof(CommunicationFile);
			var mapping2 =  db.GetMapping(type2);
			var list2 = db.Query(mapping2,"select * from \"" + mapping2.TableName + "\"", new string[]{""}).Cast<CommunicationFile>().ToList();



			dataTable = new UITableView (SingletonAcessor.Acessor.isIphone? new CGRect(10,40,355,this.View.Frame.Height - 140) : new CGRect(20,195,984,543), UITableViewStyle.Plain);
			dataTable.RowHeight = SingletonAcessor.Acessor.isIphone? 40 : 44;
			dataTable.BackgroundColor = UIColor.Clear;
			dataTable.SectionHeaderHeight =  SingletonAcessor.Acessor.isIphone? 30 : 40;
			dataTable.SeparatorStyle =  UITableViewCellSeparatorStyle.None;
			dataTable.Layer.CornerRadius = 4;
			dataTable.Layer.MasksToBounds = true;
			t.Add (dataTable);

			dataTable.Source = new DeviceTableSource (list);
			dataTable.ReloadData ();



			var cancel = new UIButton (UIButtonType.RoundedRect);
			cancel.Frame = new RectangleF (10, 620, 80, 30);
			cancel.SetTitle ("Back", UIControlState.Normal);
			cancel.TouchUpInside += delegate {
				UIView.BeginAnimations(null,IntPtr.Zero);
				UIView.SetAnimationDuration(1); 
				UIView.SetAnimationCurve(UIViewAnimationCurve.EaseInOut);
				UIView.SetAnimationTransition(UIViewAnimationTransition.FlipFromLeft,this.View,true);
				UIView.CommitAnimations();
				t.RemoveFromSuperview();	
			};
			t.Add (cancel);

			totalProperties = new UILabel (new CGRect (10,600,350,30));
			totalProperties.TextAlignment = UITextAlignment.Right;
			totalProperties.Text = "Total Properties: " + list.Count.ToString ();
			totalProperties.Font = UIFont.FromName ("Helvetica", 12);
			t.Add (totalProperties);

			totalImported = new UILabel (new CGRect (10,620,350,30));
			totalImported.TextAlignment = UITextAlignment.Right;
			totalImported.Text = "Total Imported Files: " + list2.Count.ToString ();
			totalImported.Font = UIFont.FromName ("Helvetica", 12);
			t.Add (totalImported);

//			text = new UITextView (new RectangleF(10,40,300,460));
//			StringBuilder s = new StringBuilder ("  SECONDS            SECURITY TYPE \n\n\n");
//			foreach (var l in list) 
//			{
//				s.Append ("  " +l.Seconds.ToString("000.00", CultureInfo.InvariantCulture)  + "                    " + l.SecurityType + "\n");
//			}
//			text.Text = s.ToString ();
//			text.Editable = false;
//			t.Add (text);

			UIView.BeginAnimations(null,IntPtr.Zero);
			UIView.SetAnimationDuration(1); 
			UIView.SetAnimationCurve(UIViewAnimationCurve.EaseInOut);
			UIView.SetAnimationTransition(UIViewAnimationTransition.FlipFromLeft,this.View,true);
			UIView.CommitAnimations();
			this.View.Add(t);

		}

		UIPickerView categoriesPicker;
		PickerDelegate pickerDelegate;
		PickerModelIphone model;
		List<KeywordEntry> listOfKeyword;
		List<KeywordManualEntry> listOfManualKeyword;

		void LoadListofKeyword()
		{
			var db = SingletonAcessor.Acessor.database;
			var type = typeof(KeywordEntry);
			var mapping =  db.GetMapping(type);
			listOfKeyword = db.Query(mapping,"select * from \"" + mapping.TableName + "\"", new string[]{""}).Cast<KeywordEntry>().ToList();

			var type1 = typeof(KeywordManualEntry);
			var mapping1 =  db.GetMapping(type1);
			listOfManualKeyword = db.Query(mapping1,"select * from \"" + mapping1.TableName + "\"", new string[]{""}).Cast<KeywordManualEntry>().ToList();


		}

		Top5TableSource source;
		Top5ManualTableSource manualSource;

		void ReloadTables()
		{
			LoadListofKeyword ();

			var listFiltered = listOfKeyword.Where (x => x.Category.Equals (SingletonAcessor.Acessor.listCategories [(int)categoriesPicker.SelectedRowInComponent (0)])).ToList ();
			listFiltered = listFiltered.OrderByDescending(x=>x.Rate).Take(5).ToList();
			source = new Top5TableSource (listFiltered);
			dataTable.Source = source;
			dataTable.ReloadData ();

			var listManualFiltered = listOfManualKeyword.Where (x => x.Category.Equals (SingletonAcessor.Acessor.listCategories [(int)categoriesPicker.SelectedRowInComponent (0)])).ToList ();
			listManualFiltered = listManualFiltered.OrderByDescending(x=>x.Rate).ToList();
			manualSource = new Top5ManualTableSource (listManualFiltered);
			manualSource.deletedItem+= delegate {
				dataManualTable.ReloadData ();
			};
			dataManualTable.Source = manualSource;
			dataManualTable.ReloadData ();
		}

		void CheckTables()
		{
			t = new UIView (this.View.Frame);


			UIView BackgroundImg = new UIView ();
			BackgroundImg.BackgroundColor = UIColor.White;
			nfloat BackgroundSize = (this.View.Frame.Width > this.View.Frame.Height) ? this.View.Frame.Width : this.View.Frame.Height;
			BackgroundImg.Frame = new CoreGraphics.CGRect (0.0F, 0.0F, BackgroundSize, BackgroundSize);
			this.t.AddSubview (BackgroundImg);

			var restartTable = new UIButton (UIButtonType.RoundedRect);
			restartTable.Frame = new RectangleF (120, 620, 180, 30);
			restartTable.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
			restartTable.SetTitle ("REFRESH TF-IDF TABLE", UIControlState.Normal);
			restartTable.TouchUpInside += delegate {
				CalculateKeyValues();
				ReloadTables();
			};
			t.Add (restartTable);

			var plusButton = new UIButton (UIButtonType.ContactAdd);
			plusButton.Frame = new RectangleF (320, 620, 30, 30);
			plusButton.TouchUpInside += delegate {

				var controller = new UIViewController();
				controller.View = new UIView(new CGRect(0,0,300,200));
				controller.View.BackgroundColor = UIColor.White;

				var toolbar =  new UIToolbar(new CGRect(0, 0, 300, 44));
				toolbar.Translucent = true;

				UITextField keyInput = new UITextField();
				keyInput.Frame = new CGRect(50,70,200,30);
				keyInput.Placeholder = "Enter Keyword Here";
				controller.Add(keyInput);

				UILabel value = new UILabel();
				value.Frame = new CGRect(50,140,200,30);
				value.Text = "0.75";
				value.TextAlignment = UITextAlignment.Right;
				controller.Add(value);

				var slider = new UISlider();
				slider.Frame = new CGRect(50,120,200,30);
				slider.MinValue = 0;
				slider.Value = 0.75f;
				slider.MaxValue = 1;
				slider.ValueChanged += delegate {
					value.Text = slider.Value.ToString("0.00");
				};
				controller.Add(slider);

				UIBarButtonItem	UIBarButtonItem1 = new UIBarButtonItem(UIBarButtonSystemItem.FlexibleSpace, null);
				UIBarButtonItem	UIBarButtonItem2 = new UIBarButtonItem("Add to Manual Table", UIBarButtonItemStyle.Bordered, delegate {

					var newManualEntry = new KeywordManualEntry();
					newManualEntry.Keyword = keyInput.Text;
					newManualEntry.Rate = slider.Value;
					newManualEntry.Category = SingletonAcessor.Acessor.listCategories [(int)categoriesPicker.SelectedRowInComponent (0)];
					SingletonAcessor.Acessor.database.Insert(newManualEntry);

					UIView.Animate (0.5f, 0, UIViewAnimationOptions.CurveEaseInOut ,
						() => {
							centerView.Alpha = 0f;
							totalViewRegister.Alpha = 0f;
							centerView.EndEditing(true);
						},
						null
					);

					ReloadTables();
				});
				UIBarButtonItem[] barButtonItems =  new UIBarButtonItem[] {UIBarButtonItem1, UIBarButtonItem2};

				toolbar.SetItems(barButtonItems, true);
				controller.View.AddSubview(toolbar);

				IphonePresentMenu(controller, new CGPoint ((t.Frame.Width - 300) / 2, (t.Frame.Height - 300) / 2), controller.View.Frame.Size, delegate{ });
			};
			t.Add (plusButton);


			var cancel = new UIButton (UIButtonType.RoundedRect);
			cancel.Frame = new RectangleF (10, 620, 80, 30);
			cancel.SetTitle ("Back", UIControlState.Normal);
			cancel.TouchUpInside += delegate {
				UIView.BeginAnimations(null,IntPtr.Zero);
				UIView.SetAnimationDuration(1); 
				UIView.SetAnimationCurve(UIViewAnimationCurve.EaseInOut);
				UIView.SetAnimationTransition(UIViewAnimationTransition.FlipFromLeft,this.View,true);
				UIView.CommitAnimations();
				t.RemoveFromSuperview();	
			};
			t.Add (cancel);

			categoriesPicker = new UIPickerView ();
			categoriesPicker.Frame = new CGRect (10,0,355,216);
			pickerDelegate = new PickerDelegate (SingletonAcessor.Acessor.listCategories);
			categoriesPicker.Delegate = pickerDelegate;
			model = new PickerModelIphone (SingletonAcessor.Acessor.listCategories);
			model.PickerSelected += delegate {
			
				ReloadTables();
			};
				
			categoriesPicker.Model = model;
			categoriesPicker.ReloadAllComponents ();
			t.AddSubview (categoriesPicker);

			dataTable = new UITableView (SingletonAcessor.Acessor.isIphone? new CGRect(10,200,355,180) : new CGRect(20,195,984,543), UITableViewStyle.Plain);
			dataTable.RowHeight = SingletonAcessor.Acessor.isIphone? 30 : 44;
			dataTable.BackgroundColor = UIColor.Clear;
			dataTable.SectionHeaderHeight =  SingletonAcessor.Acessor.isIphone? 30 : 40;
			dataTable.SeparatorStyle =  UITableViewCellSeparatorStyle.None;
			dataTable.Layer.CornerRadius = 4;
			dataTable.Layer.MasksToBounds = true;
			t.Add (dataTable);

			dataManualTable = new UITableView (SingletonAcessor.Acessor.isIphone? new CGRect(10,400,355,180) : new CGRect(20,195,984,543), UITableViewStyle.Plain);
			dataManualTable.RowHeight = SingletonAcessor.Acessor.isIphone? 30 : 44;
			dataManualTable.BackgroundColor = UIColor.Clear;
			dataManualTable.SectionHeaderHeight =  SingletonAcessor.Acessor.isIphone? 30 : 40;
			dataManualTable.SeparatorStyle =  UITableViewCellSeparatorStyle.None;
			dataManualTable.Layer.CornerRadius = 4;
			dataManualTable.Layer.MasksToBounds = true;
			t.Add (dataManualTable);

			ReloadTables ();

			UIView.BeginAnimations(null,IntPtr.Zero);
			UIView.SetAnimationDuration(1); 
			UIView.SetAnimationCurve(UIViewAnimationCurve.EaseInOut);
			UIView.SetAnimationTransition(UIViewAnimationTransition.FlipFromLeft,this.View,true);
			UIView.CommitAnimations();
			this.View.Add(t);

		}

		UITableView dataManualTable;

		void CheckDataStats()
		{
			t = new UIView (this.View.Frame);

			UIView BackgroundImg = new UIView ();
			BackgroundImg.BackgroundColor = UIColor.White;
			nfloat BackgroundSize = (this.View.Frame.Width > this.View.Frame.Height) ? this.View.Frame.Width : this.View.Frame.Height;
			BackgroundImg.Frame = new CoreGraphics.CGRect (0.0F, 0.0F, BackgroundSize, BackgroundSize);
			this.t.AddSubview (BackgroundImg);

			var cancel = new UIButton (UIButtonType.RoundedRect);
			cancel.Frame = new RectangleF (10, 620, 80, 30);
			cancel.SetTitle ("Back", UIControlState.Normal);
			cancel.TouchUpInside += delegate {
				UIView.BeginAnimations(null,IntPtr.Zero);
				UIView.SetAnimationDuration(1); 
				UIView.SetAnimationCurve(UIViewAnimationCurve.EaseInOut);
				UIView.SetAnimationTransition(UIViewAnimationTransition.FlipFromLeft,this.View,true);
				UIView.CommitAnimations();
				t.RemoveFromSuperview();	
			};
			t.Add (cancel);

			var db = SingletonAcessor.Acessor.database;
			var typeOfObject = typeof(PropertyValue);
			var mapping =  db.GetMapping(typeOfObject);
			List <PropertyValue> list = db.Query(mapping,"select * from \"" + mapping.TableName + "\"", new string[]{""}).Cast<PropertyValue>().ToList();


		
			StringBuilder s = new StringBuilder ();
			var o = list.GroupBy( i => i.DeviceName ).OrderByDescending(g => g.Count());

			s.AppendLine ("PROPERTIES BY DEVICE");
			s.AppendLine ();

			foreach( var grp in o)
			{
				s.AppendLine( string.Format("{0}: {1}", grp.Key, grp.Count()) );
			}

			s.AppendLine ();
			s.AppendLine ();

			s.AppendLine ("PROPERTIES BY TYPE");
			s.AppendLine ();

			var l = list.GroupBy( i => i.DeviceType ).OrderByDescending(g => g.Count());

			foreach( var grp in l)
			{
				s.AppendLine(  string.Format("{0}: {1}", grp.Key, grp.Count()) );
			}

			var textView = new UITextView ();
			textView.Text = s.ToString ();
			textView.Frame = new CGRect (10, 20, this.View.Frame.Width - 20, this.View.Frame.Height - 80);
			t.Add (textView);

			UIView.BeginAnimations(null,IntPtr.Zero);
			UIView.SetAnimationDuration(1); 
			UIView.SetAnimationCurve(UIViewAnimationCurve.EaseInOut);
			UIView.SetAnimationTransition(UIViewAnimationTransition.FlipFromLeft,this.View,true);
			UIView.CommitAnimations();
			this.View.Add(t);

		}


		UIView totalViewRegister;
		UIView totalButton;
		UIButton buttonOff;
		UIView centerView;

		public void IphonePresentMenu(UIViewController ctr, CGPoint place, CGSize size, Action action)
		{
			totalViewRegister = new UIView ();
			totalViewRegister.Frame = this.View.Frame;
			totalViewRegister.BackgroundColor = UIColor.Black;
			totalViewRegister.Alpha = 0f;
			this.View.AddSubview (totalViewRegister);

			buttonOff = new UIButton (UIButtonType.Custom);
			buttonOff.Frame = this.View.Frame;
			buttonOff.TouchUpInside += delegate {


				action();

				UIView.Animate (0.5f, 0, UIViewAnimationOptions.CurveEaseInOut ,
					() => {
						centerView.Alpha = 0f;
						totalViewRegister.Alpha = 0f;
						centerView.EndEditing(true);
					},
					null
				);
			};
			totalViewRegister.Add (buttonOff);


			centerView = new UIView ();
			centerView.Frame = new CGRect (place, size);
			centerView.Alpha = 0f;
			centerView.Layer.CornerRadius = 8;
			centerView.Layer.MasksToBounds = true;

			ctr.View.Frame = new CGRect (new CGPoint(0,0), size);
			ctr.PreferredContentSize = size;
			this.View.AddSubview (centerView);
			this.centerView.AddSubview(ctr.View);

			UIView.Animate (0.5f, 0, UIViewAnimationOptions.CurveEaseInOut ,
				() => {

					totalViewRegister.Alpha = 0.5f;
					centerView.Alpha = 1f;
				},
				null
			);
		}

		public void HideMenu()
		{
			centerView.Alpha = 0f;
			totalViewRegister.Alpha = 0f;
		}


	}
}

