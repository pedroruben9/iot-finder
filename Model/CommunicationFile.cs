﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTFinder
{
	public class CommunicationFile
    {
		public bool isXML { get; set; }

		public bool isJson { get; set; }

		public string filename { get; set; }

		public string fileContent { get; set; }

		public string DeviceName { get; set; }

    }
}
