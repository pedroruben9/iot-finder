﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTFinder
{
	public class KeywordEntry
    {

		public string Category { get; set; }

		public string Keyword { get; set; }

		public double Rate { get; set; }

		public string RateCalculus { get; set; }

    }
}
