﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTFinder
{
	public class KeywordManualEntry
    {

		public string Category { get; set; }

		public string Keyword { get; set; }

		public double Rate { get; set; }


    }
}
