﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IoTFinder
{
	public class PropertyValue
    {
		public string ParentFile { get; set; }

		public string Property { get; set; }

		public string Value { get; set; }

		public string Type { get; set; }

		public string DeviceName { get; set; }

		public string DeviceType { get; set; }

    }
}
